package com.ynavizovskyi.vinditinder.domain.model.getimages;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;

/**
 * Created by ynavi on 03.10.2017.
 */

@Getter
public class GetImagesResult {
    @SerializedName("items")
    private List<ImageItem> imageItems;
}
