package com.ynavizovskyi.vinditinder.domain.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by ynavi on 03.10.2017.
 */

@Getter
@Setter
public class User {
    private int id;
    private String name;
    private String imageUrl;
}
