package com.ynavizovskyi.vinditinder.domain;

/**
 * Created by ynavi on 03.10.2017.
 */

public interface NetworkRequestCallback<T> {
    void onSuccess(T data);

    void onError();

}
