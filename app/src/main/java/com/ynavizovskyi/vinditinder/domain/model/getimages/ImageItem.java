package com.ynavizovskyi.vinditinder.domain.model.getimages;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;

/**
 * Created by ynavi on 03.10.2017.
 */

@Getter
public class ImageItem {
    @SerializedName("media_fullsize")
    private String fullImageUrl;
}
