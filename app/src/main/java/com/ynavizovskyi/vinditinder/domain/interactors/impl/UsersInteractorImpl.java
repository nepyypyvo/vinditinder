package com.ynavizovskyi.vinditinder.domain.interactors.impl;

import com.ynavizovskyi.vinditinder.domain.Api;
import com.ynavizovskyi.vinditinder.domain.interactors.UsersInteractor;
import com.ynavizovskyi.vinditinder.domain.model.User;
import com.ynavizovskyi.vinditinder.domain.model.getimages.GetImagesResponse;
import com.ynavizovskyi.vinditinder.domain.model.getimages.ImageItem;
import com.ynavizovskyi.vinditinder.networking.RetrofitCallbackWrapper;
import com.ynavizovskyi.vinditinder.presentation.ui.views.tinder.TinderView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ynavi on 03.10.2017.
 */

public class UsersInteractorImpl implements UsersInteractor {
    private Api api;

    public UsersInteractorImpl(Api api) {
        this.api = api;
    }

    @Override
    public void getCaseStudies(final Callback callback) {
        api.getImages(new RetrofitCallbackWrapper<GetImagesResponse>() {
            @Override
            public void onSuccess(GetImagesResponse data) {
                callback.onUsersLoaded(convertResponseToUsers(data));
            }

            @Override
            public void onError() {
                callback.onError();
            }
        });
    }

    private List<User> convertResponseToUsers(GetImagesResponse response){
        List<User> users = new ArrayList<>();
        if(response == null || response.getGetImagesData() == null
                || response.getGetImagesData().getGetImagesResult() == null){
            return users;
        }
        for (ImageItem imageItem: response.getGetImagesData().getGetImagesResult().getImageItems()){
            User user = new User();
            //for some reason picasso doesn't load original urls,
            user.setImageUrl("https:" + imageItem.getFullImageUrl().split("&")[0]);
            users.add(user);
        }

        if (users.size() >2){
            User user = new User();
            user.setImageUrl("https://www.ednist.info/media/images/67322/raw/2ea177a1e7c8e0c4f28a5e1c29865d77.jpg");
            user.setId(TinderView.CardHolder.OLEN_VALERIYOVYCH_ID);
            users.add(2, user);
        }

        //If you ever happend to be reading this code, I have to apologize and say that I did this
        //only because the api service I use to get images returns improper image urls sometimes,
        //and you should never do this in real life apps
        users.remove(9);
        users.remove(12);
        users.remove(51);
        return users;
    }

}
