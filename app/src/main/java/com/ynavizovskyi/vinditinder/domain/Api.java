package com.ynavizovskyi.vinditinder.domain;

import com.ynavizovskyi.vinditinder.domain.model.getimages.GetImagesResponse;

/**
 * Created by ynavi on 03.10.2017.
 */

public interface Api {
    void getImages(NetworkRequestCallback<GetImagesResponse>  callback);
}