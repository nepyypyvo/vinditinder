package com.ynavizovskyi.vinditinder.domain.interactors;

import com.ynavizovskyi.vinditinder.domain.model.User;

import java.util.List;

/**
 * Created by ynavi on 03.10.2017.
 */

public interface UsersInteractor {
    void getCaseStudies(Callback callback);

    interface Callback {
        void onUsersLoaded(List<User> users);

        void onError();
    }
}
