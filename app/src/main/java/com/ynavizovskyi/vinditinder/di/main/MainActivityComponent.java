package com.ynavizovskyi.vinditinder.di.main;

import com.ynavizovskyi.vinditinder.di.PerActivity;
import com.ynavizovskyi.vinditinder.presentation.ui.activities.MainActivity;

import dagger.Subcomponent;

/**
 * Created by ynavi on 03.10.2017.
 */

@Subcomponent(modules = {MainActivityModule.class})
@PerActivity
public interface MainActivityComponent {
    void inject(MainActivity activity);
}
