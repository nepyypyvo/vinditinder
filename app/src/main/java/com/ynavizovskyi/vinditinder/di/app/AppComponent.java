package com.ynavizovskyi.vinditinder.di.app;

import com.ynavizovskyi.vinditinder.di.main.MainActivityComponent;
import com.ynavizovskyi.vinditinder.di.main.MainActivityModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by ynavi on 03.10.2017.
 */

@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface AppComponent {
    MainActivityComponent plus(MainActivityModule module);
}
