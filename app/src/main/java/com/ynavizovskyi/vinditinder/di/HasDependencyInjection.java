package com.ynavizovskyi.vinditinder.di;

/**
 * Created by ynavi on 03.10.2017.
 */

public interface HasDependencyInjection {
    void setupDependencies();
}