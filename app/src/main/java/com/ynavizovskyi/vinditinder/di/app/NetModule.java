package com.ynavizovskyi.vinditinder.di.app;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ynavizovskyi.vinditinder.domain.Api;
import com.ynavizovskyi.vinditinder.networking.ApiImpl;
import com.ynavizovskyi.vinditinder.networking.RetrofitApi;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ynavi on 03.10.2017.
 */

@Module
public class NetModule {
    private static final int DEFAULT_TIMEOUT_SECONDS = 60;

    public NetModule() {

    }

    @Provides
    @Singleton
    Gson providesGson() {
        Gson gson = new GsonBuilder().create();
        return gson;
    }

    @Provides
    @Singleton
    OkHttpClient providesOkhttpClient(){
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder()
                .connectTimeout(DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .addInterceptor(loggingInterceptor)
                .readTimeout(DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @Singleton
    Retrofit providesRetrofit(OkHttpClient okHttpClient, Gson gson){
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl("https://www.google.com")
                .client(okHttpClient)
                .build();
        return retrofit;
    }

    @Provides
    @Singleton
    Api providesApi(Retrofit retrofit) {
        return new ApiImpl(retrofit.create(RetrofitApi.class));
    }
}