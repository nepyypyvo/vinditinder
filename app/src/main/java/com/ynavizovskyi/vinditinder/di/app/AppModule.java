package com.ynavizovskyi.vinditinder.di.app;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.ynavizovskyi.vinditinder.VindiApp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ynavi on 03.10.2017.
 */

@Module
public class AppModule {
    private VindiApp application;

    public AppModule(VindiApp application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public VindiApp providesApplication(){
        return application;
    }

    @Provides
    @Singleton
    SharedPreferences providesSharedPreferences(VindiApp application){
        return PreferenceManager.getDefaultSharedPreferences(application);
    }

}
