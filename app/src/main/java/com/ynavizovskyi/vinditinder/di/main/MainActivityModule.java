package com.ynavizovskyi.vinditinder.di.main;

import com.ynavizovskyi.vinditinder.di.PerActivity;
import com.ynavizovskyi.vinditinder.domain.Api;
import com.ynavizovskyi.vinditinder.domain.interactors.UsersInteractor;
import com.ynavizovskyi.vinditinder.domain.interactors.impl.UsersInteractorImpl;
import com.ynavizovskyi.vinditinder.presentation.presenters.MainActivityPresenter;
import com.ynavizovskyi.vinditinder.presentation.presenters.impl.MainActivityPresenterImpl;

import javax.inject.Inject;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ynavi on 03.10.2017.
 */

@Module
public class MainActivityModule {
    private MainActivityPresenter.View view;

    @Inject
    public MainActivityModule(MainActivityPresenter.View view) {
        this.view = view;
    }

    @Provides
    @PerActivity
    UsersInteractor provideInteractor(Api api){
        return new UsersInteractorImpl(api);
    }

    @Provides
    @PerActivity
    MainActivityPresenter providePresenter(UsersInteractor usersInteractor) {
        MainActivityPresenter presenter = new MainActivityPresenterImpl(view, usersInteractor);
        return presenter;
    }
}
