package com.ynavizovskyi.vinditinder;

import android.app.Application;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.ynavizovskyi.vinditinder.di.app.AppComponent;
import com.ynavizovskyi.vinditinder.di.app.AppModule;
import com.ynavizovskyi.vinditinder.di.app.DaggerAppComponent;
import com.ynavizovskyi.vinditinder.di.app.NetModule;

import lombok.Getter;

/**
 * Created by ynavi on 03.10.2017.
 */

public class VindiApp extends Application {
    @Getter
    private static VindiApp instance;

    @Getter
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        initDagger();
        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(this));
    }

    private void initDagger(){
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(instance))
                .netModule(new NetModule())
                .build();
    }
}
