package com.ynavizovskyi.vinditinder.presentation.presenters.impl;

import com.ynavizovskyi.vinditinder.domain.interactors.UsersInteractor;
import com.ynavizovskyi.vinditinder.domain.model.User;
import com.ynavizovskyi.vinditinder.presentation.presenters.BasePresenter;
import com.ynavizovskyi.vinditinder.presentation.presenters.MainActivityPresenter;

import java.util.List;

/**
 * Created by ynavi on 03.10.2017.
 */

public class MainActivityPresenterImpl extends BasePresenter<MainActivityPresenter.View> implements MainActivityPresenter {
    UsersInteractor interactor;

    public MainActivityPresenterImpl(View view, UsersInteractor interactor) {
        super(view);
        this.interactor = interactor;
    }

    @Override
    public void getUsers() {
        getView().showProgress();
        interactor.getCaseStudies(new UsersInteractor.Callback() {
            @Override
            public void onUsersLoaded(List<User> users) {
                getView().onUsersLoaded(users);
            }

            @Override
            public void onError() {
                getView().onError();
            }
        });
    }
}
