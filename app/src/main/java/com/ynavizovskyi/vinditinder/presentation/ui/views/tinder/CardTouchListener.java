package com.ynavizovskyi.vinditinder.presentation.ui.views.tinder;

import android.animation.Animator;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.RelativeLayout;

/**
 * Created by yurii.navizovskyi on 02.10.2017.
 */

public class CardTouchListener implements View.OnTouchListener {
    private final static int TOUCH_SLOP_PIX = 10;
    private final static int MAX_CARD_ROTATION_DEGREES = 30;
    private final static float EXIT_SWIPE_PERCENT = 0.25F;
    private final static int BACK_ANIMATION_DURATION_MILLS = 150;
    private final static int OUT_ANIMATION_DURATION_MILLS = 250;
    private final View card;
    private float touchDownRawY;
    private float touchDownX;
    private float touchDownRawX;
    private float touchDownY;
    private float cardMarginTop;
    private float cardMarginRight;
    private float cardMarginBottom;
    private float cardMarginLeft;
    private FlingListener flingListener;
    private boolean isBeingAnimated;
    private int activePointerId;


    public CardTouchListener(View card, FlingListener flingListener) {
        this.card = card;
        this.flingListener = flingListener;
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) card.getLayoutParams();
        cardMarginTop = layoutParams.topMargin;
        cardMarginRight = layoutParams.rightMargin;
        cardMarginBottom = layoutParams.bottomMargin;
        cardMarginLeft = layoutParams.leftMargin;
    }


    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (isBeingAnimated) return false;
        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                onActionDown(motionEvent);
                break;
            case MotionEvent.ACTION_UP:
                onActionUp(motionEvent);
                break;
            case MotionEvent.ACTION_MOVE:
                onActionMove(motionEvent);
                break;
            case MotionEvent.ACTION_CANCEL:
//                resetCard();
                break;
        }

        return true;
    }

    private void onActionDown(final MotionEvent motionEvent) {
        activePointerId = motionEvent.getPointerId(0);
        touchDownRawX = motionEvent.getRawX();
        touchDownRawY = motionEvent.getRawY();
        touchDownX = motionEvent.getX();
        touchDownY = motionEvent.getY();
    }

    private void onActionUp(MotionEvent motionEvent) {
        float dx = motionEvent.getRawX() - touchDownRawX;
        float dy = motionEvent.getRawY() - touchDownRawY;
        if (Math.abs(dx) > card.getWidth() * EXIT_SWIPE_PERCENT) {
            animateCardOut(dx, dy);
        } else {
            flingListener.onCardSwipeCanceled(card);
            animateCardBack();
        }
    }

    private void onActionMove(MotionEvent motionEvent) {
        float dx = motionEvent.getRawX() - touchDownRawX;
        float dy = motionEvent.getRawY() - touchDownRawY;
        card.setTranslationX(dx);
        card.setTranslationY(dy);
        card.setRotation(getRotationAngle(dx));

        float percentOut = Math.abs(dx) / (card.getWidth() * EXIT_SWIPE_PERCENT);
        percentOut = percentOut > 1f ? 1f : percentOut;

        if(dx > 0){
            flingListener.onRightOutPercent(card, percentOut);
        } else {
            flingListener.onLeftOutPercent(card, percentOut);
        }
    }

    private void animateCardOut(float dx, float dy) {
        final boolean isLeft = dx < 0;
        float xDestination = (float) (card.getWidth() + cardMarginLeft + cardMarginRight
                + Math.sin(Math.toRadians(MAX_CARD_ROTATION_DEGREES)) * card.getHeight());
        xDestination = isLeft ? -xDestination : xDestination;
        isBeingAnimated = true;
        card.animate()
                .setDuration(OUT_ANIMATION_DURATION_MILLS)
                .setInterpolator(new OvershootInterpolator(1.5f))
                .x(xDestination)
                .y(dy)
                .rotation(getRotationAngle(isLeft ? -card.getHeight() : card.getHeight()))
                .setListener(new AnimationEndListener() {
                    @Override
                    public void onAnimationOver() {
                        if (isLeft) {
                            flingListener.onCardSwipedLeft(card);
                        } else {
                            flingListener.onCardSwipedRight(card);
                        }
                        isBeingAnimated = false;
                    }
                });
    }

    private void animateCardBack() {
        isBeingAnimated = true;
        card.animate()
                .setDuration(BACK_ANIMATION_DURATION_MILLS)
                .setInterpolator(new OvershootInterpolator(1.5f))
                .x(cardMarginLeft)
                .y(cardMarginTop)
                .rotation(0)
                .setListener(new AnimationEndListener() {
                    @Override
                    public void onAnimationOver() {
                        isBeingAnimated = false;
                    }
                });
    }

    private float getRotationAngle(float dx) {
        float rotationAngle = -dx / card.getWidth() * MAX_CARD_ROTATION_DEGREES;
        if (touchDownY < card.getHeight() / 2) {
            rotationAngle = -rotationAngle;
        }
        return rotationAngle;
    }


    public interface FlingListener {
        void onCardSwipedLeft(View card);

        void onCardSwipedRight(View card);

        void onCardSwipeCanceled(View card);

        void onCardSwipedUp(View card);

        void onLeftOutPercent(View card, float outPercent);

        void onRightOutPercent(View card, float outPercent);
    }

    public abstract class AnimationEndListener implements Animator.AnimatorListener {

        @Override
        public void onAnimationStart(Animator animator) {

        }

        @Override
        public void onAnimationEnd(Animator animator) {
            onAnimationOver();
        }

        @Override
        public void onAnimationCancel(Animator animator) {

        }

        @Override
        public void onAnimationRepeat(Animator animator) {

        }

        public abstract void onAnimationOver();
    }

}
