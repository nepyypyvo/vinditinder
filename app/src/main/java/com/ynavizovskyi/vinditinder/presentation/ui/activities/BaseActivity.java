package com.ynavizovskyi.vinditinder.presentation.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.ynavizovskyi.vinditinder.VindiApp;
import com.ynavizovskyi.vinditinder.di.HasDependencyInjection;
import com.ynavizovskyi.vinditinder.di.app.AppComponent;
import com.ynavizovskyi.vinditinder.presentation.presenters.Presenter;

import javax.inject.Inject;

import lombok.Getter;

/**
 * Created by ynavi on 03.10.2017.
 */

public abstract class BaseActivity<T extends Presenter> extends AppCompatActivity implements HasDependencyInjection {
    @Inject
    @Getter
    public T presententer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupDependencies();
    }

    protected AppComponent getAppComponent() {
        return VindiApp.getInstance().getAppComponent();
    }

}