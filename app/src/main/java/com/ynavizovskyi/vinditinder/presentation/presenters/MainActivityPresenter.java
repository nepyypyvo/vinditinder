package com.ynavizovskyi.vinditinder.presentation.presenters;

import com.ynavizovskyi.vinditinder.domain.model.User;

import java.util.List;

/**
 * Created by ynavi on 03.10.2017.
 */

public interface MainActivityPresenter extends Presenter{

    void getUsers();

    interface View {
        void showProgress();

        void onUsersLoaded(List<User> users);

        void onError();

    }
}
