package com.ynavizovskyi.vinditinder.presentation.ui.views.tinder;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.ynavizovskyi.vinditinder.R;
import com.ynavizovskyi.vinditinder.domain.model.User;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ynavi on 01.10.2017.
 */

public class TinderView extends RelativeLayout {
    private final static int CARD_STACK_SIZE = 4;
    private final static float INACTIVE_CARD_SCALE = 0.95f;
    private View rootView;
    private TextView likesCountTv;
    private TextView nopesCountTv;
    private Adapter adapter;
    private int likesCount;
    private int nopesCount;
    private Map<View, CardHolder> cardHolderMap;
    private FlingListener outerFlingListener;
    private View secondCard;

    public TinderView(Context context) {
        super(context);
        init();
    }

    public TinderView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TinderView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void setAdapter(Adapter adapter) {
        this.adapter = adapter;
        initCards();
    }

    public void setFlingListener(FlingListener outerFlingListener) {
        this.outerFlingListener = outerFlingListener;
    }

    private void init() {
        cardHolderMap = new HashMap<>();
        rootView = LayoutInflater.from(getContext()).inflate(R.layout.view_tinder_cards, null);
        likesCountTv = rootView.findViewById(R.id.tv_likes_count);
        nopesCountTv = rootView.findViewById(R.id.tv_nopes_count);
        addView(rootView);
    }

    private CardTouchListener.FlingListener flingListener = new CardTouchListener.FlingListener() {
        @Override
        public void onCardSwipedLeft(View card) {
            CardHolder cardHolder = cardHolderMap.get(card);
            if (cardHolder.getUser().getId() == CardHolder.OLEN_VALERIYOVYCH_ID) {
                likesCountTv.setText(Integer.toString(++likesCount));
            } else {
                nopesCountTv.setText(Integer.toString(++nopesCount));
            }
            recycleCard(card);
            if (outerFlingListener != null) {
                outerFlingListener.onCardSwipedLeft(cardHolder.getUser());
            }
        }

        @Override
        public void onCardSwipedRight(View card) {
            CardHolder cardHolder = cardHolderMap.get(card);
            likesCountTv.setText(Integer.toString(++likesCount));
            recycleCard(card);
            if (outerFlingListener != null) {
                outerFlingListener.onCardSwipedRight(cardHolder.getUser());
            }
        }

        @Override
        public void onCardSwipedUp(View card) {

        }

        @Override
        public void onCardSwipeCanceled(View card) {
            cardHolderMap.get(card).resetIndicators();
        }

        @Override
        public void onLeftOutPercent(View card, float outPercent) {
            cardHolderMap.get(card).setNopeIndicatorAlpha(outPercent);
            animateSecondCard(outPercent);
        }

        @Override
        public void onRightOutPercent(View card, float outPercent) {
            cardHolderMap.get(card).setLikeIndicatorAlpha(outPercent);
            animateSecondCard(outPercent);
        }
    };

    private void animateSecondCard(float percentOut) {
        if (secondCard == null) {
            return;
        }
        float scale = INACTIVE_CARD_SCALE + (1f - INACTIVE_CARD_SCALE) * percentOut;
        secondCard.setScaleX(scale);
        secondCard.setScaleY(scale);
    }

    private void recycleCard(View card) {
        User user = adapter.getNextUser();
        CardHolder oldCardHolder = cardHolderMap.get(card);
        oldCardHolder.deactivateCard();
        removeView(card);
        resetCard(card);

        //-2 because we have one extra child apart from cards
        CardHolder newCardHolder = cardHolderMap.get(getChildAt(getChildCount() - 2));
        if (newCardHolder != null) {
            newCardHolder.activateCard();
        }
        CardHolder secondCardHolder = cardHolderMap.get(getChildAt(getChildCount() - 3));
        if (secondCardHolder != null) {
            secondCard = cardHolderMap.get(getChildAt(getChildCount() - 3)).getCard();
        }
        if (user == null) {
            return;
        }
        addView(card, 0, card.getLayoutParams());
        oldCardHolder.populateCard(user);
    }

    private void resetCard(View card) {
        card.setTranslationX(0);
        card.setTranslationY(0);
        card.setRotation(0);
    }


    private void initCards() {
        for (int i = 0; i < CARD_STACK_SIZE; i++) {
            User user = adapter.getNextUser();
            if (user == null) {
                return;
            }
            View card = LayoutInflater.from(getContext()).inflate(R.layout.view_card, this, false);
            CardHolder cardHolder = new CardHolder(card, flingListener);
            cardHolder.populateCard(user);
            if (i == 0) {
                cardHolder.activateCard();
            } else if (i == 1) {
                secondCard = card;
                card.setScaleX(INACTIVE_CARD_SCALE);
                card.setScaleY(INACTIVE_CARD_SCALE);
            } else {
                card.setScaleX(INACTIVE_CARD_SCALE);
                card.setScaleY(INACTIVE_CARD_SCALE);
            }

            cardHolderMap.put(card, cardHolder);
            addView(card, 0, card.getLayoutParams());
        }
    }

    public interface Adapter {
        User getNextUser();
    }

    public interface FlingListener {
        void onCardSwipedLeft(User user);

        void onCardSwipedRight(User user);

        void onCardSwipedUp(User user);
    }

    public class CardHolder {
        public static final int OLEN_VALERIYOVYCH_ID = 333;
        private View card;
        private CardTouchListener.FlingListener flingListener;
        private ImageView profilePictureIv;
        private View likeIndicatorView;
        private View nopeIndicatorView;
        private View loveIndicatorView;
        private View progressBar;
        private User user;

        public CardHolder(View card, CardTouchListener.FlingListener flingListener) {
            this.card = card;
            this.flingListener = flingListener;
            profilePictureIv = card.findViewById(R.id.iv_profile_picture);
            likeIndicatorView = card.findViewById(R.id.v_like_indicator);
            loveIndicatorView = card.findViewById(R.id.v_love_indicator);
            nopeIndicatorView = card.findViewById(R.id.v_nope_indicator);
            progressBar = card.findViewById(R.id.pb_card);
        }

        public User getUser() {
            return user;
        }

        public View getCard() {
            return card;
        }

        public void populateCard(User user) {
            this.user = user;
            resetIndicators();
            profilePictureIv.setVisibility(GONE);
            progressBar.setVisibility(VISIBLE);
            Log.v("dlskfjlsdkjf", user.getImageUrl());
            ImageLoader.getInstance().loadImage(user.getImageUrl(), new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String s, View view) {

                }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason) {
                    progressBar.setVisibility(GONE);

                }

                @Override
                public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                    progressBar.setVisibility(GONE);
                    profilePictureIv.setVisibility(VISIBLE);
                    profilePictureIv.setImageBitmap(bitmap);
                }

                @Override
                public void onLoadingCancelled(String s, View view) {
                    progressBar.setVisibility(GONE);

                }
            });

        }

        public void resetIndicators() {
            likeIndicatorView.setAlpha(0);
            nopeIndicatorView.setAlpha(0);
            loveIndicatorView.setAlpha(0);
        }

        public void activateCard() {
            card.setOnTouchListener(new CardTouchListener(card, flingListener));
        }

        public void deactivateCard() {
            card.setOnTouchListener(null);
            setCardScale(INACTIVE_CARD_SCALE);
            card.setScaleX(INACTIVE_CARD_SCALE);
            card.setScaleY(INACTIVE_CARD_SCALE);
        }

        public void setCardScale(float scale){

        }

        public void setLikeIndicatorAlpha(float alpha) {
            likeIndicatorView.setAlpha(alpha);
        }

        public void setNopeIndicatorAlpha(float alpha) {
            if (user.getId() == OLEN_VALERIYOVYCH_ID) {
                loveIndicatorView.setAlpha(alpha);
            } else {
                nopeIndicatorView.setAlpha(alpha);
            }
        }
    }

}
