package com.ynavizovskyi.vinditinder.presentation.ui.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.ynavizovskyi.vinditinder.R;
import com.ynavizovskyi.vinditinder.di.main.MainActivityModule;
import com.ynavizovskyi.vinditinder.domain.model.User;
import com.ynavizovskyi.vinditinder.presentation.presenters.MainActivityPresenter;
import com.ynavizovskyi.vinditinder.presentation.ui.views.tinder.TinderView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity<MainActivityPresenter> implements MainActivityPresenter.View {
    @BindView(R.id.view_tinder)
    TinderView tinderView;
    @BindView(R.id.pb_cards)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initTinderView();
        getPresententer().getUsers();
    }

    @Override
    public void setupDependencies() {
        getAppComponent().plus(new MainActivityModule(this)).inject(this);
    }

    private void initTinderView(){
        tinderView.setFlingListener(new TinderView.FlingListener() {
            @Override
            public void onCardSwipedLeft(User user) {

            }

            @Override
            public void onCardSwipedRight(User user) {

            }

            @Override
            public void onCardSwipedUp(User user) {

            }
        });
    }

    @Override
    public void showProgress() {
        tinderView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onUsersLoaded(List<User> users) {
        hideProgress();
        tinderView.setAdapter(new CardAdapter(users));
    }

    private void hideProgress(){
        progressBar.setVisibility(View.GONE);
        tinderView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onError() {

    }

    private class CardAdapter implements TinderView.Adapter{
        private List<User> data;
        private int pointer;

        public CardAdapter(List<User> data) {
            this.data = data;
        }

        public List<User> getData() {
            return data;
        }

        @Override
        public User getNextUser() {
            User user = null;
            if(pointer < data.size()){
                user = data.get(pointer++);
            }
            return user;
        }
    }
}
