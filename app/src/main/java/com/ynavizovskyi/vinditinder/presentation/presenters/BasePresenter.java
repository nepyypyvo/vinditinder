package com.ynavizovskyi.vinditinder.presentation.presenters;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by ynavi on 03.10.2017.
 */

public abstract class BasePresenter <T>{
    @Setter
    @Getter
    private T view;

    public BasePresenter(T view) {
        this.view = view;
    }

}