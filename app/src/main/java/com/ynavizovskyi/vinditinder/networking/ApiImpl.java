package com.ynavizovskyi.vinditinder.networking;

import com.ynavizovskyi.vinditinder.domain.Api;
import com.ynavizovskyi.vinditinder.domain.NetworkRequestCallback;
import com.ynavizovskyi.vinditinder.domain.model.getimages.GetImagesResponse;

import retrofit2.Callback;

/**
 * Created by ynavi on 03.10.2017.
 */

public class ApiImpl implements Api {
    private RetrofitApi retrofitApi;

    public ApiImpl(RetrofitApi retrofitApi) {
        this.retrofitApi = retrofitApi;
    }

    @Override
    public void getImages(NetworkRequestCallback<GetImagesResponse> callback) {
        retrofitApi.getImages(100, 1, "pretty girls").enqueue((Callback<GetImagesResponse>) callback);
    }
}
