package com.ynavizovskyi.vinditinder.networking;

import com.ynavizovskyi.vinditinder.domain.model.getimages.GetImagesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by ynavi on 03.10.2017.
 */

public interface RetrofitApi {
    String GET_IMAGES = "https://api.qwant.com/api/search/images";

    @GET(GET_IMAGES)
    @Headers({
            "Content-Type: application/json",
            "Accept-Language: uk"
    })
    Call<GetImagesResponse> getImages(@Query("count") int count, @Query("offset") int offset
            , @Query("q") String query);

}