package com.ynavizovskyi.vinditinder.networking;

import com.ynavizovskyi.vinditinder.domain.NetworkRequestCallback;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ynavi on 03.10.2017.
 */

public abstract class RetrofitCallbackWrapper<T> implements Callback<T>, NetworkRequestCallback<T> {

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.isSuccessful()){
            onSuccess(response.body());
        } else {
            onError();
        }

    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        t.printStackTrace();
        onError();
    }
}